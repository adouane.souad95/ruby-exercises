class Microwave
    attr_reader :time
    def initialize(m)
      @time = m
    end
    def pad_int_to_2_chars(n)
      n < 10 ? ("0" + n.to_s) : n.to_s
    end
    def timer
      mt = time.divmod(100)
      if mt[1] > 59
        mt[0] += 1
        mt[1] = mt[1].divmod(60)[1]
      end
      "#{pad_int_to_2_chars(mt[0])}:#{pad_int_to_2_chars(mt[01])}"
      
    end
  end
  
