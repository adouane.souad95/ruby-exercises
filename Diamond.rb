class Diamond

    def self.make_diamond(letter)
      return "A\n" if letter == "A"
      range_arr = ("A"..letter).to_a
      pred_letter = range_arr[range_arr.size - 2]
      row_column_size = range_arr.size * 2 - 1
      triangle = triangle_for_range(("A"..pred_letter), row_column_size)
      middle_row = " " * row_column_size
      middle_row[0] = letter
      middle_row[row_column_size - 1] = letter
      triangle.reverse + "\n" + middle_row + "\n" + triangle + "\n"
    end
    
    def self.triangle_for_range(r, row_column_size)
      r.to_a.reverse.each_with_index.map do |l, i|
        s = " " * row_column_size
        pos1 = -i - 2
        pos2 = i + 1
        s[pos1] = l
        s[pos2] = l
        s
      end.join("\n")
    end
  end